##################################################################################
# IAM RESOURCES
##################################################################################

# Users #

resource "aws_iam_user" "amir_chaudhri" {
    name = "amir_chaudhri"
    path = "/"
}

# Groups #
resource "aws_iam_group" "PowerUserAccessGroup" {
    name = "PowerUserAccessGroup"
    path = "/"
}

# Group Members #
resource "aws_iam_group_membership" "PowerUserAccessGroupMembers" {
    name  = "PowerUserAccessGroupMembers"
    users = ["${aws_iam_user.amir_chaudhri.name}"]
    group = "${aws_iam_group.PowerUserAccessGroup.name}"
}



# Police Attachment #

resource "aws_iam_policy_attachment" "PowerUserAccessPolicyAttachment" {
    name       = "PowerUserAccessPolicyAttachment"
    policy_arn = "arn:aws:iam::aws:policy/PowerUserAccess"
    groups     = ["${aws_iam_group.PowerUserAccessGroup.name}"]
    users      = []
    roles      = []
}