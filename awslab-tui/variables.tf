##################################################################################
# VARIABLES
##################################################################################

variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "network_address_space" {
  default = "172.16.0.0/16"
}

variable "billing_code_tag" {}
variable "environment_tag" {}

variable "subnet_count" {
  default = 1
}

variable "database_instance" {
  default = "ami-023c8dbf8268fb3ca"
}
variable "database_instance_count" {
  default = 1
}

variable "webserver_instance" {
  default = "ami-023c8dbf8268fb3ca"
}

variable "webserver_instance_count" {
  default = 1
}

variable "key_name" {
  default = "awslab_tui_us_east_2"
}