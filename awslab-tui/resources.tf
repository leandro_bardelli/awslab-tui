##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-2" 
}


##################################################################################
# DATA
##################################################################################

data "aws_availability_zones" "available" {}

##################################################################################
# RESOURCES
##################################################################################

# NETWORKING #
module "vpc" {
  source = "Modules/vpc"
  name = "${var.environment_tag}"

  cidr = "${var.network_address_space}"
  azs = "${slice(data.aws_availability_zones.available.names,0,var.subnet_count)}"
  tags {
    BillingCode        = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }
}

# SECURITY GROUPS #

#Security Group Public Ports: HTTP/ICMP/SSH
resource "aws_security_group" "public-ports-sg" {
  name        = "public-ports-sg"
  vpc_id      = "${module.vpc.vpc_id}"

  #Allow HTTP from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Allow ICMP from anywhere
  ingress {
    from_port   = 0
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Allow SSH from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.environment_tag}-public-ports-sg"
    BillingCode        = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }

}

# Security Group Private Ports: Custom DB Port/ICMP/SSH
resource "aws_security_group" "private-ports-sg" {
  name        = "private-ports-sg"
  vpc_id      = "${module.vpc.vpc_id}"

  ##Allow Custom DB from internal subnet
  ingress {
    from_port   = 3110
    to_port     = 3110
    protocol    = "tcp"
    cidr_blocks = ["172.16.1.0/24"]
  }

  #Allow ICMP from anywhere
  ingress {
    from_port   = 0
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Allow SSH from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["172.16.1.0/24"]
  }

  tags {
    Name = "${var.environment_tag}-private-ports-sg"
    BillingCode        = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }

}

# INSTANCES #

# Webserver Instance
resource "aws_instance" "webserver" {
  count = "${var.webserver_instance_count}"
  ami           = "${var.webserver_instance}"
  instance_type = "t2.micro"
  subnet_id     = "${element(module.vpc.public_subnets,count.index % var.subnet_count)}"
  vpc_security_group_ids = ["${aws_security_group.public-ports-sg.id}"]
  key_name        = "${var.key_name}"

  tags {
    Name = "${var.environment_tag}-webserver-${count.index + 1}"
    BillingCode        = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }

}
# DB Instance
resource "aws_instance" "database" {
  count = "${var.database_instance_count}"
  ami           = "${var.database_instance}"
  instance_type = "t2.micro"
  associate_public_ip_address = false
  subnet_id     = "${element(module.vpc.public_subnets,count.index % var.subnet_count)}"
  vpc_security_group_ids = ["${aws_security_group.private-ports-sg.id}"]
  key_name        = "${var.key_name}"

  tags {
    Name = "${var.environment_tag}-database-${count.index + 1}"
    BillingCode        = "${var.billing_code_tag}"
    Environment = "${var.environment_tag}"
  }

}