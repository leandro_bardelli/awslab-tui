# AWS LAB Terraform. 

This repository contains Terraform core and one module to deal with VPC resources.  

#### Here a simple example whith a how-to run along the variables included:  


```
$ cd awslab-tui

$ terraform plan -var-file='../terraform.tfvars' -var-file='Development/development.tfvars' -state='Development/development.state'

$ terraform apply -var-file='../terraform.tfvars' -var-file='Development/development.tfvars' -state='Development/development.state'
```